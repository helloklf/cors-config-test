var http = require('http');
var https = require('https');
var url=require('url');
var fs=require('fs');
var mine=require('./mine').types;
var path=require('path');
var router=require('./router');

var apiConfig=require('../config')

exports.routers = function(request, response) {
    var pathname = url.parse(request.url).pathname;
    var realPath = path.join("assets", pathname);
    if (/^\/api\/.*$/i.test(pathname)){
        response.writeHead(200, { "Content-Type": "application/json" })
        switch (pathname.toLowerCase()) {
            case "/api/get-api-list" : {
                response.write(JSON.stringify(apiConfig, null, 2));
    
                response.end();
                break
            }
        }
    } else if(/^\/proxy\/.*$/i.test(pathname)) {
        console.log('proxy ' + pathname)
        var requestApi = url.parse(request.url);
        var proxyRequest = (request.protocol == "https:" ? https : http).request({
            hostname: "127.0.0.1",
            path: "/index.html/#/" + requestApi.path,
            port: 8080,
            protocol: requestApi.protocol,
            method: request.method,
        },function(res){
            res.on('data',function(data){
                console.log('proxy ' + pathname + " " + res.statusCode)
                response.writeHead(res.statusCode, res.headers);
                response.write("*********************************");
                response.write(data);
            });
            res.on("end", function() {
                console.log('proxy ' + pathname + " end")
                response.end();
            });
        });
        proxyRequest.on("error", function(err) {
            response.writeHead(500, {});
            response.write("");
            response.end();
        });
        proxyRequest.on("timeout", function(err) {
            response.writeHead(500, {});
            response.write("");
            response.end();
        });
        proxyRequest.end();

    } else {
        response.writeHead(404, {
            'Content-Type': 'text/plain'
        });
        response.write("This request URL " + pathname + " was not found on this server.");
    }
}